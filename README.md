# bazel-coverage-report

[![pipeline status](https://gitlab.gaponov.dev/viking/bazel-coverage-report/badges/master/pipeline.svg)](https://gitlab.gaponov.dev/viking/bazel-coverage-report/commits/master)

`bazel-coverage-report` is a multi-language coverage report generator for
[Bazel](https://bazel.build).  It is based on [genhtml](https://github.com/linux-test-project/lcov.git).

## Usage

In your WORKSPACE:
```python

load("@bazel_tools//tools/build_defs/repo:git.bzl", "new_git_repository")

new_git_repository(
    name = "bazel_coverage_report",
    build_file_content = """
cc_library(
    name = "bazel_coverage_report",
    srcs = ["..."],
    hdrs = ["..."],
""",
    remote = "https://gitlab.gaponov.dev:50443/viking/bazel-coverage-report.git",
    branch = "master",
)
load("@bazel_coverage_report//report:defs.bzl", "bazel_coverage_report_repositories")
bazel_coverage_report_repositories()  # lcov, ...
```

Then:

1. Generate coverage data with `bazel coverage //... --instrumentation_filter=/path[/:]`
2. Build the coverage report generator: `bazel build @bazel_coverage_report//report:bin`
3. Generate the report: `bazel-bin/external/bazel_coverage_report/report/bin --dest_dir=<dest dir>`

## Supported languages

- C, C++
- Golang
- Javascript, Typescript
- Java, Kotlin
- R

See [`./WORKSPACE`](./WORKSPACE) for the version of the rules that are supported.  Some of
these versions are pending Push Requests.
